# envixty-dotfiles
These are my dotfiles

In order to install my dotfiles do this:

First, clone my repo:

```zsh
git clone https://github.com/redkittty/envixty-dotfiles.git
```

Then, cd into it:

```zsh
cd envixty-dotfiles
```

After that, give execute permissions to bootstrap.sh:

```zsh
chmod +x bootstrap.sh
```

Finally, execute bootstrap.sh:

```zsh
./bootstrap.sh
```

To do it in one line, do this

```zsh
git clone https://github.com/redkittty/envixty-dotfiles.git && cd envixty-dotfiles && chmod +x bootstrap.sh && ./bootstrap.sh
```


My dotfiles include configs for

- Kitty Terminal

- Hyprland

- ZSH

- Git

- Neovim
